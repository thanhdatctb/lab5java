/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bai1;

import java.io.File;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author thanh
 */
public class WriteObject implements Serializable{
    public static void main(String[] args) {
        //ObjectIOExample objectIO = new ObjectIOExample();
        Scanner input = new Scanner(System.in);  // Create a Scanner object
        List<Employee> list = new ArrayList<Employee>(); 
        for(int i = 1; i<=3; i++){
            Scanner input1 = new Scanner(System.in);  
            System.out.print("Nhập id nhân viên thứ "+i+": ");
            int id = input1.nextInt();
            Scanner input2 = new Scanner(System.in);  
            System.out.print("Nhập tên nhân viên thứ "+i+": ");
            String name = input2.nextLine();
            Scanner input3 = new Scanner(System.in);  
            System.out.print("Nhập tuổi nhân viên thứ "+i+": ");
            int salary = input3.nextInt();
            Employee l = new Employee(id, name, salary);
            list.add(l);
            
        }
        Employee student = new Employee(1,"Frost",22);
        WriteObjectToFile(list);
        //WriteObjectToFile(student);
    }

    private static void WriteObjectToFile(Object serObj) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
         try {
 
            FileOutputStream fileOut = new FileOutputStream("nhanvien.bin");
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(serObj);
            objectOut.close();
            System.out.println("The Object  was succesfully written to a file");
 
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
