/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bai1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author thanh
 */
public class WriteToFile {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);  // Create a Scanner object
        List<Employee> list = new ArrayList<Employee>(); 
        for(int i = 1; i<=3; i++){
            Scanner input1 = new Scanner(System.in);  
            System.out.print("Nhập id nhân viên thứ "+i+": ");
            int id = input1.nextInt();
            Scanner input2 = new Scanner(System.in);  
            System.out.print("Nhập tên nhân viên thứ "+i+": ");
            String name = input2.nextLine();
            Scanner input3 = new Scanner(System.in);  
            System.out.print("Nhập tuổi nhân viên thứ "+i+": ");
            int salary = input3.nextInt();
            Employee l = new Employee(id, name, salary);
            list.add(l);
            
        }
        try {
            File myObj = new File("worker.txt");
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (Exception e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        String content = "";
        for(Employee e: list){
            content+=e.toString()+"\n";
        }
        try {
            FileWriter myWriter = new FileWriter("worker.txt");
            myWriter.write(content);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
    public void writeToWorker() {
        
    }
}
