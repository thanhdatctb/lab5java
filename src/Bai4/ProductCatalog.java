/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bai4;

import Bai2.Employee;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author thanh
 */
public class ProductCatalog {

    private static HashMap<String, Product> pList = new HashMap<>();
    private static String fname = "TONKHO.DAT";

    public static void add() {
        Product p = new Product();
        p = p.input();
        pList.put(Integer.toString(pList.size()), p);
    }

    public static void set(String sID, float sQty, boolean isReceive) {
        if (isReceive) {
            for (int i = 0; i < pList.size(); i++) {
                if (pList.get(i).getId() == sID) {
                    pList.get(i).setIn_qty(pList.get(i).getIn_qty() + sQty);
                }
            }
            return;
        }
        for (int i = 0; i < pList.size(); i++) {
            if (pList.get(i).getId() == sID) {
                pList.get(i).setOut_qty(pList.get(Integer.toString(i)).getOut_qty() - sQty);
            }
        }

    }

    public static void display(HashMap<String, Product> pList ) {
        for (int i = 0; i < pList.size(); i++) {
            System.out.println(pList.get(Integer.toString(i)).output());
        }
    }

    public static void saveFile(HashMap<String, Product> pList ) {
        try {
            File myObj = new File("TONKHO.DAT");
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (Exception e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        String content = "";
        for (int i = 0; i < pList.size(); i++) {

            content += "\n" + pList.get(Integer.toString(i)).output();
        }
        try {
            FileWriter myWriter = new FileWriter("TONKHO.DAT");
            myWriter.write(content);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        int option;
        //readFile();
        do {
            System.out.println("1.Thêm sản phẩm mới");
            System.out.println("2.Xuất kho 1 sản phẩm");
            System.out.println("3.Nhập kho 1 sản phẩm");
            System.out.println("4.In báo cáo tồn kho");
            System.out.println("5.Lưu ds tồn kho sản phẩm vô file có tên TONKHO.DAT");
            System.out.println("6.Thoát");

            System.out.print("Lựa chọn của bạn: ");
            Scanner sc = new Scanner(System.in);
            option = sc.nextInt();
            switch (option) {
                case 1:
                    add();
                    break;
                case 2:
                    System.out.print("Nhập id cần thêm: ");
                    Scanner sc1 = new Scanner(System.in);
                    String id = sc1.nextLine();
                    System.out.print("Nhập số lượng cần thêm: ");
                    Scanner sc2 = new Scanner(System.in);
                    int sl = sc1.nextInt();
                    set(id,sl,true);
                    break;
                case 3:
                    System.out.print("Nhập id cần xuất: ");
                    Scanner sc3 = new Scanner(System.in);
                    String idx = sc3.nextLine();
                    System.out.print("Nhập số lượng cần xuất: ");
                    Scanner sc4 = new Scanner(System.in);
                    int slx = sc4.nextInt();
                    set(idx,slx,false);
                    break;
                case 4:
                    display(pList );
                    break;
                case 5:
                    saveFile(pList);
                    break;
                case 6:
                    break;
                default:
                    System.out.println("Lựa chọn không phù hợp");
            }
        } while (option != 6);

    }

}
