/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bai4;

import Bai2.Employee;
import java.util.Scanner;

/**
 *
 * @author thanh
 */
public class Product {

    private String id, name;
    private int price;
    private float in_qty = 0, out_qty = 0;

    public Product(String id, String name, int price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public float getIn_qty() {
        return in_qty;
    }

    public void setIn_qty(float in_qty) {
        this.in_qty = in_qty;
    }

    public float getOut_qty() {
        return out_qty;
    }

    public void setOut_qty(float out_qty) {
        this.out_qty = out_qty;
    }

    public Product input() {
        String id, name;
        int price;
        float in_qty = 0, out_qty = 0;

        do {
            Scanner sc = new Scanner(System.in);
            System.out.print("Nhập id: ");
            id = sc.nextLine();
            //System.out.println(id.substring(0, 2));
        } while (!id.substring(0, 1).endsWith("P"));
        do {
            Scanner sc = new Scanner(System.in);
            System.out.print("Nhập Tên: ");
            name = sc.nextLine();

        } while (name.length() == 0);
        do {
            Scanner sc = new Scanner(System.in);
            System.out.print("Nhập giá: ");
            price = sc.nextInt();

        } while (price <= 0);
        do {
            Scanner sc = new Scanner(System.in);
            System.out.print("Nhập in_qty : ");
            in_qty  = sc.nextInt();

        } while (1!=1);
        do {
            Scanner sc = new Scanner(System.in);
            System.out.print("Nhập out_qty: ");
            out_qty = sc.nextInt();

        } while (1!=1);
        Product pro = new Product(id, name, price);
        pro.setIn_qty(in_qty);
        pro.setOut_qty(out_qty);
        return pro;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", in_qty=" + in_qty + ", out_qty=" + out_qty + '}';
    }
    
    public String output(){
       return("id=" + id + ", name=" + name + ", price=" + price + ", in_qty=" + in_qty + ", out_qty=" + out_qty+", qoh =" +  (in_qty - out_qty) + ", amt =" + (in_qty - out_qty)*price);
    }
    public Product(){
        
    }
}
