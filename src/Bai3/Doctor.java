/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bai3;

import java.io.Serializable;

/**
 *
 * @author thanh
 */
public class Doctor implements Serializable {
    private String id, name, specialization;
    private int availHours;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public int getAvailHours() {
        return availHours;
    }

    public void setAvailHours(int availHours) {
        this.availHours = availHours;
    }
    
    public Doctor(String id, String name, String specialization, int availHours) {
        this.id = id;
        this.name = name;
        this.specialization = specialization;
        this.availHours = availHours;
    }

    @Override
    public String toString() {
        return id + "," + name + "," + specialization + "," + availHours;
    }
    
    
}
