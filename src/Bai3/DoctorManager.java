/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bai3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thanh
 */
public class DoctorManager {

    private static TreeMap<String, Doctor> doctorList = new TreeMap<>();
    private static String fname;

    public DoctorManager() {
        
    }
    
    public static void add(TreeMap<String, Doctor> doctorList) {
        System.out.print("Id: ");
        Scanner sc1 = new Scanner(System.in);
        String id = sc1.nextLine();
        System.out.print("name: ");
        Scanner sc2 = new Scanner(System.in);
        String name = sc2.nextLine();
        System.out.print("specialization: ");
        Scanner sc3 = new Scanner(System.in);
        String specialization = sc3.nextLine();
         System.out.print("availHours: ");
        Scanner sc4 = new Scanner(System.in);
        int availHours = sc4.nextInt();
        Doctor doctor = new Doctor(id, name, specialization, availHours);
        doctorList.put("", doctor);
    }

    private static void printTreeMap(TreeMap<String, Doctor> tree) {
        Set<Doctor> keys = (Set<Doctor>) tree.values();

        //iterate using forEach
        keys.forEach(key -> {
            System.out.println(key);
        });
    }

    public static void display() {
        printTreeMap(doctorList);
    }

    public static void display(String id, TreeMap<String, Doctor> tree) {
        Collection<Doctor> keys = tree.values();

        //iterate using forEach
        keys.forEach(key -> {
            if (key.getId().endsWith(id)) {
                System.out.println(key);
            }
        });
    }

    public static void readFile() {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("doctor.txt"));
            System.out.println("" +  ois.readObject());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DoctorManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DoctorManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DoctorManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void saveFile() {
        try {

            FileOutputStream fileOut = new FileOutputStream("doctor.txt");
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(doctorList);
            objectOut.close();
            System.out.println("The Object  was succesfully written to a file");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        int option;
        //readFile();
        do {
            System.out.println("1.Đăng ký bác sĩ mới");
            System.out.println("2.Tìm kiếm bác sĩ theo tên");
            System.out.println("3.In danh sách bác sĩ");
            System.out.println("4.Ghi File.");
            System.out.println("5.Thoát.");

            System.out.print("Lựa chọn của bạn: ");
            Scanner sc = new Scanner(System.in);
            option = sc.nextInt();
            switch (option) {
                case 1:
                    add(doctorList);
                    break;
                case 2:
                    System.out.print("Nhập id cần tìm: ");
                    Scanner sc1 = new Scanner(System.in);
                    String name = sc1.nextLine();
                    display(name,doctorList);
                    break;
                case 3:
                    display();
                    break;
                case 4:
                    saveFile();
                    break;
                case 5:

                    break;
               
                default:
                    System.out.println("Lựa chọn không phù hợp");
            }
        } while (option != 6);

    }
}
