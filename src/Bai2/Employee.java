/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bai2;

import java.util.Scanner;

/**
 *
 * @author thanh
 */
public class Employee {

    private String id, name, address;
    private int salary, joinedYear;

    public Employee(String id, String name, String address, int salary, int joinedYear) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.salary = salary;
        this.joinedYear = joinedYear;
    }
    public Employee(){
        
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getJoinedYear() {
        return joinedYear;
    }

    public void setJoinedYear(int joinedYear) {
        this.joinedYear = joinedYear;
    }

    @Override
    public String toString() {
        return id + "," + name + "," + address + "," + salary + "," + joinedYear;
    }

    public Employee input() {
        String id, name, address;
        int salary, joinedYear;
        do {
            Scanner sc = new Scanner(System.in);
            System.out.print("Nhập id: ");
            id = sc.nextLine();
            //System.out.println(id.substring(0, 2));
        } while (!id.substring(0, 2).endsWith("EM"));
        do {
            Scanner sc = new Scanner(System.in);
            System.out.print("Nhập Tên: ");
            name = sc.nextLine();

        } while (name.length() == 0);
        do {
            Scanner sc = new Scanner(System.in);
            System.out.print("Nhập Địa chỉa: ");
            address = sc.nextLine();

        } while (name.length() == 0);
        do {
            Scanner sc = new Scanner(System.in);
            System.out.print("Nhập Lương: ");
            salary = sc.nextInt();

        } while (salary<100||salary>10000);
        do {
            Scanner sc = new Scanner(System.in);
            System.out.print("Nhập năm vào làm: ");
            joinedYear = sc.nextInt();

        } while (joinedYear<1980||salary>2016);
        Employee em = new Employee(id, name, address, salary, joinedYear);
        return em;

    }

}
