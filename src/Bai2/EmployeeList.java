/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bai2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Scanner;

/**
 *
 * @author thanh
 */
public class EmployeeList {

    /**
     * @param args the command line arguments
     */
    private static HashSet<Employee> empList = new HashSet<>();

    public EmployeeList() {
        try{
            this.empList = ReadFileText();
        }catch(Exception ex){
            this.empList = new HashSet<>();
        }
        
    }

    public void addEmp(Employee em) {
        for (Employee e : this.empList) {
            if (e.getId() == em.getId()) {
                return;
            }
        }
        empList.add(em);
    }

    public static boolean searchByName(String name, HashSet<Employee> empList) {
        HashSet<Employee> newHash = new HashSet<>();
        for (Employee e : empList) {
            if (e.getName().contains(name)) {
                newHash.add(e);
            }
        }
        //HashSet<Employee> hashSet = (HashSet<Employee>) empList.stream().filter(s -> s.getName().contains(name));
        printHashSet(newHash);
        return true;
    }

    public static void display() {
        printHashSet(empList);
    }

    private static void printHashSet(HashSet<Employee> emSet) {
        for (Employee em : emSet) {
            System.out.println(em.toString());
        }
    }

    public static void display(int years, HashSet<Employee> empList) {

        Date d = new Date();
        int year = d.getYear();
        int currentYear = year + 1900;
        HashSet<Employee> newList = new HashSet<Employee>();
        for (Employee s : empList) {
            if (currentYear - s.getJoinedYear() >= year) {
                newList.add(s);
            }
        }
        //HashSet<Employee> em = (HashSet<Employee>) empList.stream().filter(s -> currentYear - s.getJoinedYear() >= year);
        printHashSet(newList);
    }

    public static void writeFileText(HashSet<Employee> empList) {
        try {
            File myObj = new File("NV.txt");
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (Exception e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        String content = "";
        for (Employee e : empList) {
            content += e.toString() + "\n";
        }
        try {
            FileWriter myWriter = new FileWriter("NV.txt");
            myWriter.write(content);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public HashSet<Employee> ReadFileText() {
        HashSet<Employee> list = new HashSet<Employee>();
        try {
            File myObj = new File("NV.txt");
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String path[] = data.split(",");
                Employee e = new Employee(path[0], path[1], path[2], Integer.parseInt(path[3]), Integer.parseInt(path[4]));
            }
            myReader.close();
            
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return list;
    }

    public static void main(String[] args) {
        // TODO code application logic here
        int option;
        do {
            System.out.println("1.Thêm nhân viên mới\n");
            System.out.println("2.Tìm kiếm nhân viên theo tên\n");
            System.out.println("3.Liệt kê tất cả nhân viên trong công ty\n");
            System.out.println("4.Liệt kê các nhân viên có thâm niên công tác tùy chọn\n");
            System.out.println("5.Ghi File\n");
            System.out.println("6.Thoát\n");

            System.out.print("Lựa chọn của bạn: ");
            Scanner sc = new Scanner(System.in);
            option = sc.nextInt();
            switch (option) {
                case 1:
                    Employee em = new Employee().input();
                    empList.add(em);
                    break;
                case 2:
                    System.out.print("Nhập tên cần tìm: ");
                    Scanner sc1 = new Scanner(System.in);
                    String name = sc1.nextLine();
                    searchByName(name, empList);
                    break;
                case 3:
                    display();
                    break;
                case 4:
                    System.out.print("Nhập năm công tác: ");
                    Scanner sc2 = new Scanner(System.in);
                    int year = sc2.nextInt();
                    display(year, empList);
                    break;
                case 5:
                    writeFileText(empList);
                    break;
                case 6:
                    break;
                default:
                    System.out.println("Lựa chọn không phù hợp");
            }
        } while (option != 6);

    }

}
